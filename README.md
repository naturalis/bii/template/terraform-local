# Terraform local

This is an example of a terraform project which can be used
for local development.
For now this is Linux only and the readme is written for people running Ubuntu.
On mac your milage may vary, but some of these machines are libvirt capable.

## Install

To install this project on your machine you must have a
machine with kvm capabilities.


1. You can check this by running:

```
grep "vmx" /proc/cpuinfo
```

2. Update and upgrade your system.

```
sudo su
apt-get update && apt-get upgrade
```

3. Install kvm

```
sudo apt install libvirt-clients libvirt-daemon-system libvirt-daemon virtinst bridge-utils qemu qemu-kvm 
```

4. Everything okay?

```
kvm-ok
```

5. Install virt-manager

```
sudo apt install virt-manager
```

6. Install ssh-askpass

```
sudo apt install ssh-askpass
```

7. Disable app armor and make libvirt **run for your own user**
and change these values in `/etc/libvirt/qemu.conf`

```
   security_driver = "none"
   user = "<yourid>"
   group = "libvirt"
```

8. Add your own user to the kvm en libvirt groups:

```
sudo usermod -a -G libvirt <your id>
sudo usermod -a -G kvm <your id>
```

9. Restart

10. Install terraform using this guide

https://computingforgeeks.com/how-to-install-terraform-on-ubuntu/

Or follow these steps:

```
sudo su
apt install  software-properties-common gnupg2 curl
curl https://apt.releases.hashicorp.com/gpg | gpg --dearmor > hashicorp.gpg
install -o root -g root -m 644 hashicorp.gpg /etc/apt/trusted.gpg.d/
apt update
apt install terraform
```

11. Check if terraform is installed:

```
terraform --version
```

12. You are ready to go!

## Creating your local virtual machine

Go into the `local` part of this repository.

1. Add your public key to the `ssh_authorized` keys part in `cloud_init.cfg`.
2. Change the address in the `network_interface` part of `main.tf`, by default it is `10.10.10.101` (optional).
3. Initialize the terraform configuration:

```
terraform init
```

4. Plan the configuration:

```
terraform plan
```

5. Apply the configuration:

```
terraform apply
```

If everything is okay, you can now login to your new local virtual machine:

```
ssh ubuntu@10.10.10.101
```

You can also start **Virtual Machine Manager** and see the machine running in libvirt.


## Destroying your machine

Everytime you have done your development/testing, you can remove your configuration with:

```
terraform destroy
```

Please note that when creating a new machine after destroying you probably need to clear
your ssh fingerprints when you want to login to the newly created machine again:

```
ssh-keygen -f "~/.ssh/known_hosts" -R "10.10.10.101"
```


## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).
The pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

- `pre-commit autoupdate`
- `pre-commit install`
