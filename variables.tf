variable "MEMORY_SIZE" {
  default = 1024 * 4
  type    = string
}

variable "VCPU_SIZE" {
  default = 4
  type    = number
}

variable "VM_COUNT" {
  default = 1
  type    = number
}

variable "VM_USER" {
  default = "ubuntu"
  type    = string
}

variable "VM_HOSTNAME" {
  default = "ubuntu-vm"
  type    = string
}

variable "VM_IMG_URL" {
  default = "https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img"
  type    = string
}

variable "VM_VOLUME_SIZE" {
  default = 1025 * 1024 * 1024 * 10
  type    = number
}

variable "VM_IMG_FORMAT" {
  default = "qcow2"
  type    = string
}

variable "VM_CIDR_RANGE" {
  default = "10.10.10.10/24"
  type    = string
}

variable "LIBVIRT_DISK_PATH" {
  description = "path for libvirt pool"
  default     = "/opt/libvirt"
}
